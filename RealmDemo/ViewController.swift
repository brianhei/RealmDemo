//
//  ViewController.swift
//  RealmDemo
//
//  Created by Brian Chung on 29/12/15.
//  Copyright © 2015 Brian Chung. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    let presentPersonListViewSegueIdentifier = "PersonListViewSegueIdentifier"
    
    @IBOutlet var lastNameTextField : UITextField!
    @IBOutlet var firstNameTextField : UITextField!
    @IBOutlet var ageTextField : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("Realm default path:\(Realm.Configuration.defaultConfiguration.path!)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addButtonClick(sender : AnyObject) {
        
        print("last name text value: \(lastNameTextField.text)")
        if lastNameTextField.text!.isEmpty {
            showAlertMessage("Please fill in last name")
        }

        print("first name text value: \(firstNameTextField.text)")
        if firstNameTextField.text!.isEmpty {
            showAlertMessage("Please fill in first name")
        }
        
        print("age text value: \(ageTextField.text)")
        if ageTextField.text!.isEmpty {
            showAlertMessage("Please fill in age")
        }
        
        let person = Person()
        person.lastName = lastNameTextField.text
        person.firstName = firstNameTextField.text
        person.age = Int(ageTextField.text!)!
        
        self.addPersonToRealm(person)
        self.resetButtonClick(self)
    }
    
    @IBAction func viewPersonDataButtonClick(sender : AnyObject) {
        //self.performSegueWithIdentifier(presentPersonListViewSegueIdentifier, sender: self)
        if let viewController = self.storyboard?.instantiateViewControllerWithIdentifier("PersonListViewController") as? PersonListViewController {
            let navController = UINavigationController(rootViewController: viewController)
            self.presentViewController(navController, animated: true, completion: nil)
        }
    }
    
    @IBAction func cleaerCacheButtonClick(sender : AnyObject) {
        do {
            try NSFileManager.defaultManager().removeItemAtPath(Realm.Configuration.defaultConfiguration.path!)
        } catch {}
    }
    
    @IBAction func resetButtonClick(sender : AnyObject) {
        let textFieldArray = [lastNameTextField, firstNameTextField, ageTextField]
        
        for textField in textFieldArray {
            textField.text = ""
        }
    }
    
    
    private func showAlertMessage(message : String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil)
        alertController.addAction(okAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func addPersonToRealm(person : Person) {
        let realm = try! Realm()
        realm.beginWrite()
        realm.add(person)
        try! realm.commitWrite()
    }

    
}

