//
//  PersonListViewController.swift
//  RealmDemo
//
//  Created by Brian Chung on 29/12/15.
//  Copyright © 2015 Brian Chung. All rights reserved.
//

import UIKit
import RealmSwift

class PersonListViewController: UITableViewController {
    
    let realm = try! Realm()
    let datasource = try! Realm().objects(Person).sorted("lastName", ascending: true)
    var editButton : UIBarButtonItem!
    var doneButton : UIBarButtonItem!
    var notificationToken: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        notificationToken = realm.addNotificationBlock { [unowned self] note, realm in
            self.tableView.reloadData()
        }
        tableView.reloadData()
    }

    private func setupUI() {
        // Do any additional setup after loading the view.
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("backButtonClick"))
        editButton = UIBarButtonItem(title: "Edit", style: UIBarButtonItemStyle.Plain, target:  self, action: Selector("editButtonClick"))
        doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Plain, target:  self, action: Selector("doneButtonClick"))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.rightBarButtonItem = editButton
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "PersonViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        let person = datasource[indexPath.row]
        cell.textLabel?.text = person.lastName! + " " + person.firstName! + " " + String(person.age)

        return cell
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            realm.beginWrite()
            realm.delete(datasource[indexPath.row])
            try! realm.commitWrite()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        //show dialog to let user to update
        
        let person = datasource[indexPath.row]
        
        let alert = UIAlertController(title: nil, message: "Update person", preferredStyle:
            UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler { lastNameTextField in
            lastNameTextField.placeholder = "Last Name"
            lastNameTextField.text = person.lastName!
        }
        
        alert.addTextFieldWithConfigurationHandler { firstNameTextField in
            firstNameTextField.placeholder = "First Name"
            firstNameTextField.text = person.firstName!
        }
        
        alert.addTextFieldWithConfigurationHandler { ageTextField in
            ageTextField.placeholder = "Age"
            ageTextField.text = String(person.age)
        }
        
        let updateAction = UIAlertAction(title: "Update", style: UIAlertActionStyle.Default) { alertAction in
            //update action did click
            try! self.realm.write {
                //assume all text fields contain value
                person.lastName = alert.textFields![0].text
                person.firstName = alert.textFields![1].text
                person.age = Int(alert.textFields![2].text!)!
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { alertAction in
            print("cancel action")
        }
        
        alert.addAction(updateAction)
        alert.addAction(cancelAction)
        
        self.presentViewController(alert, animated: true, completion: nil)
        
    }
    
    private func setRightButtonInNavbar() {
        if tableView.editing {
            self.navigationItem.rightBarButtonItem = doneButton
        } else {
            self.navigationItem.rightBarButtonItem = editButton
        }
    }
    
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
    
    @objc
    private func backButtonClick() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @objc
    private func doneButtonClick() {
        tableView.setEditing(false, animated: true)
        self.setRightButtonInNavbar()
    }

    @objc
    private func editButtonClick() {
        tableView.setEditing(true, animated: true)
        self.setRightButtonInNavbar()
    }
    

}
