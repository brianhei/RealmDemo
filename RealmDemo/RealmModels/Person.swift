//
//  Person.swift
//  RealmDemo
//
//  Created by Brian Chung on 29/12/15.
//  Copyright © 2015 Brian Chung. All rights reserved.
//

import Foundation
import RealmSwift

class Person: Object {
    dynamic var lastName : String? = nil
    dynamic var firstName : String? = nil
    dynamic var age = 0
    
// Specify properties to ignore (Realm won't persist these)
    
//  override static func ignoredProperties() -> [String] {
//    return []
//  }
}
